<?php
declare(strict_types=1);

namespace MailMonitor\Tests;

use Aws\Sns\Exception\InvalidSnsMessageException;
use MailMonitor\Entity\Bounce;
use MailMonitor\Entity\EmailStatus;
use MailMonitor\Handler\NotificationHandler;
use MailMonitor\Manager\EmailStatusManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

class HandlerTest extends TestCase
{
    public function getDeliveryMessage(): array
    {
        $content = file_get_contents(__DIR__ . '/fixtures/success.json');

        return [[$content]];
    }

    /**
     * @dataProvider getDeliveryMessage
     */
    public function test_handleDelivery(string $content): void
    {
        $emailStatus = new EmailStatus('test@example.com');
        $emailManager = $this->createConfiguredMock(EmailStatusManager::class, [
            'loadOrCreateEmailStatus' => $emailStatus
        ]);

        $handler = new NotificationHandler($emailManager, new NullLogger());
        $handler->handleRequest($content, false);

        $this->assertEquals(1, $emailStatus->getDeliveries()->count());
        $this->assertNotNull($emailStatus->getLastTimeDelivered());
    }

    public function getBounceMessage(): array
    {
        $content = file_get_contents(__DIR__ . '/fixtures/bounce.json');

        return [[$content]];
    }

    /**
     * @dataProvider getBounceMessage
     */
    public function test_handleBounce(string $content): void
    {
        $emailStatus = new EmailStatus('test@example.com');
        $emailManager = $this->createConfiguredMock(EmailStatusManager::class, [
            'loadOrCreateEmailStatus' => $emailStatus
        ]);

        $handler = new NotificationHandler($emailManager, new NullLogger());
        $handler->handleRequest($content, false);

        $this->assertEquals(1, $emailStatus->getBounces()->count());
        $this->assertEquals(1, $emailStatus->getHardBouncesCount());
        $this->assertEquals(0, $emailStatus->getSoftBouncesCount());
        $this->assertNotNull($emailStatus->getLastTimeBounced());
        $this->assertEquals(Bounce::TYPE_PERMANENT, $emailStatus->getLastBounceType());
    }

    public function getComplaintMessage(): array
    {
        $content = file_get_contents(__DIR__ . '/fixtures/complaint.json');

        return [[$content]];
    }

    /**
     * @dataProvider getComplaintMessage
     */
    public function test_handleComplaint(string $content): void
    {
        $emailStatus = new EmailStatus('test@example.com');
        $emailManager = $this->createConfiguredMock(EmailStatusManager::class, [
            'loadOrCreateEmailStatus' => $emailStatus
        ]);

        $handler = new NotificationHandler($emailManager, new NullLogger());
        $handler->handleRequest($content, false);

        $this->assertEquals(1, $emailStatus->getComplaints()->count());
        $this->assertNotNull($emailStatus->getLastTimeComplained());
    }

    public function test_handleEmpty(): void
    {
        $emailStatus = new EmailStatus('test@example.com');
        $emailManager = $this->createConfiguredMock(EmailStatusManager::class, [
            'loadOrCreateEmailStatus' => $emailStatus
        ]);
        $handler = new NotificationHandler($emailManager, new NullLogger());

        $this->expectException(InvalidSnsMessageException::class);
        $handler->handleRequest('');
    }
}