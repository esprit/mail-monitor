<?php
declare(strict_types=1);

namespace MailMonitor\Controller;

use Aws\Sns\Exception\InvalidSnsMessageException;
use Doctrine\ORM\EntityManagerInterface;
use MailMonitor\Handler\NotificationHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EndpointController
{
    private $handler;
    private $em;
    private $logger;

    public function __construct(NotificationHandler $handler, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->handler = $handler;
        $this->em = $em;
        $this->logger = $logger;
    }

    public function endpoint(Request $request): Response
    {
        if (false === $request->isMethod('POST')) {
            return new Response('Only POST requests are accepted.', 405);
        }

        if (!$content = $request->getContent()) {
            return new Response('Empty POST is not accepted.', 400);
        }

        $content = (string)$request->getContent();
        $this->logger->debug('Aws SNS raw request content: ' . $content);

        try {
            $this->handler->handleRequest($content);
            $this->em->flush();

        } catch (InvalidSnsMessageException $e) {
            $this->logger->error('Invalid SNS message: ' . $e->getMessage());
            return new Response('Invalid request', 400);
        }

        return new Response();
    }
}
