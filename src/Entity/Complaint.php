<?php
declare(strict_types=1);

namespace MailMonitor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @see http://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#complaint-object
 *
 * @ORM\Table(name="mail_monitor_complaints")
 * @ORM\Entity()
 */
class Complaint
{
    const TYPE_ABUSE = 'abuse';
    const TYPE_AUTH_FAILURE = 'auth-failure';
    const TYPE_FRAUD = 'fraud';
    const TYPE_NOT_SPAM = 'not-spam';
    const TYPE_OTHER = 'other';
    const TYPE_VIRUS = 'virus';

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var EmailStatus
     * @ORM\ManyToOne(targetEntity="EmailStatus", inversedBy="complaints")
     * @ORM\JoinColumn(referencedColumnName="address")
     */
    private $emailStatus;

    /**
     * The date and time at which the bounce was sent (in ISO8601 format).
     *
     * Note that this is the time at which the notification was sent by the ISP, and not the time at which it was
     * received by Amazon SES.
     *
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $timestamp;

    /**
     * The value of the User-Agent field from the feedback report.
     *
     * This indicates the name and version of the system that generated the report.
     *
     * @var string|null
     * @ORM\Column(nullable=true)
     */
    private $userAgent;

    /**
     * The value of the Feedback-Type field from the feedback report received from the ISP.
     *
     * This contains the type of feedback.
     *
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $complaintFeedbackType;

    public function __construct(EmailStatus $emailStatus, \DateTimeImmutable $timestamp, ?string $userAgent, ?string $complaintFeedbackType)
    {
        $this->emailStatus = $emailStatus;
        $this->timestamp = $timestamp;
        $this->userAgent = $userAgent;
        $this->complaintFeedbackType = $complaintFeedbackType;
    }
}
