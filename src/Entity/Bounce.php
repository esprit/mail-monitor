<?php
declare(strict_types=1);

namespace MailMonitor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @see http://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#bounce-object
 *
 * @ORM\Table(name="mail_monitor_bounces")
 * @ORM\Entity()
 */
class Bounce
{
    /** Hard bounces and subtypes */
    const TYPE_PERMANENT = 'Permanent';
    const TYPE_PERM_GENERAL = 'General';
    const TYPE_PERM_NOEMAIL = 'NoEmail';
    const TYPE_PERM_SUPPRESSED = 'Suppressed';

    /** Soft bunces and subtypes */
    const TYPE_TRANSIENT = 'Transient';
    const TYPE_TRANS_GENERAL = 'General';
    const TYPE_TRANS_BOXFULL = 'MailboxFull';
    const TYPE_TRANS_TOOLARGE = 'MessageTooLarge';
    const TYPE_TRANS_CONTREJECTED = 'ContentRejected';
    const TYPE_TRANS_ATTACHREJECTED = 'AttachmentRejected';

    /** Undetermined bounces */
    const TYPE_UNDETERMINED = 'Undetermined';

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var EmailStatus
     * @ORM\ManyToOne(targetEntity="EmailStatus", inversedBy="bounces")
     * @ORM\JoinColumn(referencedColumnName="address")
     */
    private $emailStatus;

    /**
     * The date and time at which the bounce was sent (in ISO8601 format).
     *
     * Note that this is the time at which the notification was sent by the ISP, and not the time at which it was
     * received by Amazon SES.
     *
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $timestamp;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $subType;

    /**
     * The value of the Action field from the DSN.
     *
     * This indicates the action performed by the Reporting-MTA as a result of its attempt to deliver the message to
     * this recipient.
     *
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $action;

    /**
     * The value of the EmailStatus field from the DSN.
     *
     * This is the per-recipient transport-independent status code that indicates the delivery status of the message.
     *
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $status;

    /**
     * The status code issued by the reporting MTA.
     *
     * This is the value of the Diagnostic-Code field from the DSN. This field may be absent in the DSN (and therefore
     * also absent in the JSON).
     *
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $diagnosticCode;

    public function __construct(EmailStatus $emailStatus, \DateTimeImmutable $time, string $type, string $subType, ?string $action, ?string $status, ?string $diagnosticCode)
    {
        $this->emailStatus = $emailStatus;
        $this->timestamp = $time;
        $this->type = $type;
        $this->subType = $subType;
        $this->action = $action;
        $this->status = $status;
        $this->diagnosticCode = $diagnosticCode;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getTimestamp(): \DateTimeImmutable
    {
        return $this->timestamp;
    }
}
