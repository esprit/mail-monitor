<?php
declare(strict_types=1);

namespace MailMonitor\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="mail_monitor_email_statuses")
 * @ORM\Entity()
 */
class EmailStatus
{
    /**
     * @var string|null
     * @ORM\Column(type="string", unique=true)
     * @ORM\Id
     */
    private $address;

    /**
     * @var Collection<int, Bounce>
     * @ORM\OneToMany(targetEntity="Bounce", mappedBy="emailStatus", cascade={"persist"})
     */
    private $bounces;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $hardBouncesCount = 0;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $softBouncesCount = 0;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastBounceType;

    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $lastTimeBounced;

    /**
     * @var Collection<int, Complaint>
     * @ORM\OneToMany(targetEntity="Complaint", mappedBy="emailStatus", cascade={"persist"})
     */
    private $complaints;

    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $lastTimeComplained;

    /**
     * @var Collection<int, Delivery>
     * @ORM\OneToMany(targetEntity="Delivery", mappedBy="emailStatus", cascade={"persist"})
     */
    private $deliveries;

    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $lastTimeDelivered;

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->address = mb_strtolower($email);
        $this->bounces = new ArrayCollection();
        $this->complaints = new ArrayCollection();
        $this->deliveries = new ArrayCollection();
    }

    /**
     * @param mixed[]|null $dsn
     */
    public function addBounce(\DateTimeImmutable $timestamp, string $type, string $subtype, ?array $dsn): void
    {
        $bounce = new Bounce($this, $timestamp, $type, $subtype, $dsn['action'] ?? null, $dsn['status'] ?? null, $dsn['diagnosticCode'] ?? null);

        $this->bounces->add($bounce);
        $this->lastBounceType = $type;
        $this->lastTimeBounced = $timestamp;

        if (Bounce::TYPE_PERMANENT === $type) {
            ++$this->hardBouncesCount;
        }

        if (Bounce::TYPE_TRANSIENT === $subtype) {
            ++$this->softBouncesCount;
        }
    }

    public function addComplaint(\DateTimeImmutable $timestamp, ?string $userAgent, ?string $type): void
    {
        $complaint = new Complaint($this, $timestamp, $userAgent, $type);

        $this->complaints->add($complaint);
        $this->lastTimeComplained = $timestamp;
    }

    public function addDelivery(\DateTimeImmutable $timestamp, string $smtpResponse): void
    {
        $delivery = new Delivery($this, $timestamp, $smtpResponse);

        $this->deliveries->add($delivery);
        $this->lastTimeDelivered = $timestamp;
    }

    /** @return Collection<int, Delivery> */
    public function getDeliveries(): Collection
    {
        return $this->deliveries;
    }

    public function getLastTimeDelivered(): ?\DateTimeImmutable
    {
        return $this->lastTimeDelivered;
    }

    /** @return Collection<int, Bounce> */
    public function getBounces(): Collection
    {
        return $this->bounces;
    }

    public function getHardBouncesCount(): int
    {
        return $this->hardBouncesCount;
    }

    public function getSoftBouncesCount(): int
    {
        return $this->softBouncesCount;
    }

    public function getLastTimeBounced(): ?\DateTimeImmutable
    {
        return $this->lastTimeBounced;
    }

    public function getLastBounceType(): ?string
    {
        return $this->lastBounceType;
    }

    /** @return Collection<int, Complaint> */
    public function getComplaints(): Collection
    {
        return $this->complaints;
    }

    public function getLastTimeComplained(): ?\DateTimeImmutable
    {
        return $this->lastTimeComplained;
    }
}
