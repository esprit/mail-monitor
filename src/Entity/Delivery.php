<?php
declare(strict_types=1);

namespace MailMonitor\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @see https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#delivery-object
 *
 * @ORM\Table(name="mail_monitor_deliveries")
 * @ORM\Entity()
 */
class Delivery
{
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var EmailStatus
     * @ORM\ManyToOne(targetEntity="EmailStatus", inversedBy="deliveries")
     * @ORM\JoinColumn(referencedColumnName="address")
     */
    private $emailStatus;

    /**
     * The time Amazon SES delivered the email to the recipient's mail server (in ISO8601 format).
     *
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $timestamp;

    /**
     * The SMTP response message of the remote ISP that accepted the email from Amazon SES.
     *
     * This message will vary by email, by receiving mail server, and by receiving ISP.
     *
     * @var string
     * @ORM\Column(type="text")
     */
    private $smtpResponse;

    public function __construct(EmailStatus $emailStatus, \DateTimeImmutable $timestamp, string $smtpResponse)
    {
        $this->emailStatus = $emailStatus;
        $this->timestamp = $timestamp;
        $this->smtpResponse = $smtpResponse;
    }
}
