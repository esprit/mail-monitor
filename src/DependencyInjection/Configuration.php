<?php

namespace MailMonitor\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mail_monitor');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('cache')->defaultValue(null)->end()
            ->end()
        ;

        return $treeBuilder;
    }
}