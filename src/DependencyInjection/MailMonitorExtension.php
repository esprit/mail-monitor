<?php
declare(strict_types=1);

namespace MailMonitor\DependencyInjection;

use MailMonitor\Handler\NotificationHandler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;

class MailMonitorExtension extends Extension
{
    /** @param array<mixed> $configs */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if ($cache = $config['cache']) {
            $definition = $container->getDefinition(NotificationHandler::class);
            $reference = new Reference($cache);

            $definition->addMethodCall('setCache', [$reference]);
        }
    }
}