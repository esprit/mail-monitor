<?php
declare(strict_types=1);

namespace MailMonitor\Handler;

use Aws\Sns\Exception\InvalidSnsMessageException;
use Aws\Sns\MessageValidator;
use MailMonitor\Manager\EmailStatusManager;
use MailMonitor\Model\BounceMessage;
use MailMonitor\Model\ComplaintMessage;
use MailMonitor\Model\DeliveryMessage;
use MailMonitor\Model\Message;
use MailMonitor\Model\MessageException;
use MailMonitor\Model\MessageFactory;
use Psr\Log\LoggerInterface;
use Aws\Sns\Message as Notification;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class NotificationHandler
{
    private $emailStatusManager;
    private $logger;
    private $cache;

    private const NOTIFICATION = 'Notification';
    private const SUBSCRIPTION_CONFIRMATION = 'SubscriptionConfirmation';

    /** @var string[] */
    private static $allowedTypes = [
        self::NOTIFICATION,
        self::SUBSCRIPTION_CONFIRMATION
    ];

    private const CACHE_TIME = 86400;

    public function __construct(EmailStatusManager $emailStatusManager, LoggerInterface $logger)
    {
        $this->emailStatusManager = $emailStatusManager;
        $this->logger = $logger;
    }

    public function setCache(CacheInterface $cache): void
    {
        $this->cache = $cache;
    }

    private function getCertClient(): ?callable
    {
        if (null === $this->cache) {
            return null;
        }

        return function($path) {

            return $this->cache->get(base64_encode($path), function (ItemInterface $item) use ($path) {
                $item->expiresAfter(self::CACHE_TIME);

                // same as here @see vendor/aws/aws-php-sns-message-validator/src/MessageValidator.php:68
                return @ \file_get_contents($path);
            });
        };
    }

    /**
     * @param string $content
     * @throws InvalidSnsMessageException
     */
    public function handleRequest(string $content, bool $validate = true): void
    {
        try {
            $notification = Notification::fromJsonString($content);

            if ($validate) {
                $validator = new MessageValidator($this->getCertClient());
                $validator->validate($notification);
            }

            $this->handleNotification($notification->toArray());

        } catch (\RuntimeException|\InvalidArgumentException|MessageException $e) {
            throw new InvalidSnsMessageException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /** @param array<mixed> $notification */
    protected function handleNotification(array $notification): void
    {
        if (! in_array($notification['Type'], self::$allowedTypes)) {
            throw new MessageException("Unexpected notification type: " . $notification['Type']);
        }

        if ($notification['Type'] === self::SUBSCRIPTION_CONFIRMATION) {
            $this->logger->alert(sprintf(
                "!!! You have chosen to subscribe to the topic arn:aws:sns:us-west-2:376067477311:report. "
                . "To confirm the subscription, visit the [%s] !!!",
                $notification['SubscribeURL']));
            return;
        }

        $message = MessageFactory::fromNotification($notification)->getMessage();

        $this->handleMessage($message);
    }


    /** @param mixed[]|string $recipient */
    private function getEmail($recipient): string
    {
        if (is_array($recipient)) {
            return $recipient['emailAddress'];
        }

        return (string) $recipient;
    }

    protected function handleMessage(Message $message): void
    {
        foreach ($message->getRecipients() as $recipient) {

            $emailStatus = $this->emailStatusManager->loadOrCreateEmailStatus($this->getEmail($recipient));

            $timestamp = $message->timestamp();

            if ($message instanceof DeliveryMessage) {
                $emailStatus->addDelivery($timestamp, $message->smtpResponse());
                continue;
            }

            if ($message instanceof BounceMessage) {
                $emailStatus->addBounce($timestamp, $message->bounceType(), $message->bounceSubType(), $recipient);
                continue;
            }

            if ($message instanceof ComplaintMessage) {
                $emailStatus->addComplaint($timestamp, $message->userAgent(), $message->complaintFeedbackType());
                continue;
            }
        }
    }
}