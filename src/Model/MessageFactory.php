<?php
declare(strict_types=1);

namespace MailMonitor\Model;

class MessageFactory
{
    const DELIVERY = 'Delivery';
    const BOUNCE = 'Bounce';
    const COMPLAINT = 'Complaint';

    /** @var mixed[] */
    private $data;

    /** @param array<mixed> $notification */
    final public static function fromNotification(array $notification): MessageFactory
    {
        $message = json_decode($notification['Message'], true);

        return new self($message);
    }

    /** @param mixed[] $data */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getMessage(): Message
    {
        $type = $this->data['notificationType'];

        if ($type === self::DELIVERY) {
            return new DeliveryMessage($this->data['delivery']);
        }

        if ($type === self::BOUNCE) {
            return new BounceMessage($this->data['bounce']);
        }

        if ($type === self::COMPLAINT) {
            return new ComplaintMessage($this->data['complaint']);
        }

        throw new MessageException("Unexpected message type: " . $type);
    }
}