<?php
declare(strict_types=1);

namespace MailMonitor\Model;

class BounceMessage extends Message
{
    public function bounceType(): string
    {
        return $this->data['bounceType'];
    }

    public function bounceSubType(): string
    {
        return $this->data['bounceSubType'];
    }

    public function getRecipients(): array
    {
        return $this->data['bouncedRecipients'];
    }
}