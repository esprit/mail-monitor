<?php
declare(strict_types=1);

namespace MailMonitor\Model;

class DeliveryMessage extends Message
{
    public function smtpResponse(): string
    {
        return $this->data['smtpResponse'];
    }

    public function getRecipients(): array
    {
        return $this->data['recipients'];
    }
}