<?php
declare(strict_types=1);

namespace MailMonitor\Model;

class ComplaintMessage extends Message
{
    public function userAgent(): ?string
    {
        return $this->data['userAgent'] ?? null;
    }

    public function complaintFeedbackType(): ?string
    {
        return $this->data['complaintFeedbackType'] ?? null;
    }

    public function getRecipients(): array
    {
        return $this->data['complainedRecipients'];
    }
}