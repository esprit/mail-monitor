<?php
declare(strict_types=1);

namespace MailMonitor\Model;

abstract class Message
{
    /** @var mixed[] */
    protected $data;

    /** @param mixed[] $data */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function timestamp(): \DateTimeImmutable
    {
        return new \DateTimeImmutable($this->data['timestamp']);
    }

    /** @return mixed[] */
    abstract public function getRecipients(): array;
}