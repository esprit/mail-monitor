<?php
declare(strict_types=1);

namespace MailMonitor\Manager;

use Doctrine\ORM\EntityManagerInterface;
use MailMonitor\Entity\EmailStatus;

class EmailStatusManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createEmailStatus(string $emailAddress): EmailStatus
    {
        $email = new EmailStatus($emailAddress);
        $this->entityManager->persist($email);

        return $email;
    }

    public function loadEmailStatus(string $emailAddress): ?EmailStatus
    {
        /** @var EmailStatus|null $email */
        $email = $this->entityManager->getRepository(EmailStatus::class)->find(mb_strtolower($emailAddress));

        return $email;
    }

    public function loadOrCreateEmailStatus(string $emailAddress): EmailStatus
    {
        $email = $this->loadEmailStatus($emailAddress);

        if (null === $email) {
            $email = $this->createEmailStatus($emailAddress);
        }

        return $email;
    }
}
