CONSOLE := $(shell which bin/console)

help:
	@grep '^[^#\.[:space:]][a-z-]*:' Makefile

test:
	@vendor/bin/phpunit tests

check:
	@vendor/bin/phpstan analyse
